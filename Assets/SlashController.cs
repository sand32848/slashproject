using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SlashController : MonoBehaviour
{
    [SerializeField] private float slashRange;
    [SerializeField] private float slashCooldown;
    [SerializeField] private int slashDamage;
    [SerializeField] private Transform slashPoint;
    [SerializeField] private LayerMask enemyLayer;

    private void OnEnable()
    {
        GlobalInputController.Instance.playerAction.Player.Fire.started += onSlash;
    }

    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.Player.Fire.started -= onSlash;
    }

    private void Update()
    {
        Vector2 slashDir = ((Vector2)Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>()) - (Vector2)slashPoint.position).normalized;

        Debug.DrawRay(transform.position, slashDir * slashRange, Color.red);
    }

    private void onSlash(InputAction.CallbackContext context)
    {
        Vector2 slashDir = ((Vector2)Camera.main.ScreenToWorldPoint(GlobalInputController.Instance.playerAction.Player.MousePos.ReadValue<Vector2>()) - (Vector2)slashPoint.position).normalized;
        RaycastHit2D hit2D = Physics2D.Raycast(slashPoint.transform.position, slashDir,slashRange,enemyLayer);

        if (hit2D)
        {
            if(hit2D.transform.TryGetComponent(out Health health))
            {
                health.reduceHealth(slashDamage);
            }
        }
    }
}
