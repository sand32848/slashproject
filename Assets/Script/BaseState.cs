using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class BaseState
{
    protected bool _isRootState = false;
    protected StateMachine _ctx;
    protected PlayerStateFactory _factory;
    protected BaseState _currentSubState;
    protected BaseState _currentSuperState;
    public BaseState(StateMachine currentContext,PlayerStateFactory currentFactory)
    {
        _ctx = currentContext;
        _factory = currentFactory;
    }

    public abstract void EnterState();
    public abstract void UpdateState();
    public abstract void ExitState();
    public abstract void CheckSwitchStates();
    public abstract void InitializeSubState();

    public void UpdateStates() {
        UpdateState();
        _currentSubState?.UpdateStates();
    }
    protected void SwitchState(BaseState newState) {
        ExitState();

        newState.EnterState();

        if (_isRootState)
        {
            _ctx.CurrentState = newState;
        }else if(_currentSuperState != null)
        {
            _currentSuperState.SetSubState(newState);
        }

    }
    protected void SetSuperState(BaseState newSuperState) 
    { _currentSuperState = newSuperState; 
    }
    protected void SetSubState(BaseState newSubState) 
    { _currentSubState = newSubState;newSubState.SetSuperState(this); 
    }
}
