using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundState : BaseState
{
    public GroundState(StateMachine currentContext,PlayerStateFactory playerStateFactory): base(currentContext,playerStateFactory)
    {
        _isRootState = true;
        InitializeSubState();
    }

    public override void CheckSwitchStates()
    {
        if (_ctx.isJumping)
        {
            SwitchState(_factory.Jump());
        }
    }

    public override void EnterState()
    {
       
    }

    public override void ExitState()
    {
      
    }

    public override void InitializeSubState()
    {
       if(!_ctx.IsMovePressed)
        {
            SetSubState(_factory.Idle());
        } 
       else
        {
            SetSubState(_factory.Move());
        }
    }

    public override void UpdateState()
    {
        CheckSwitchStates();
    }


}
