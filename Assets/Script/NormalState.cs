using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalState : BaseState
{
    public NormalState(StateMachine currentContext, PlayerStateFactory currentFactory) : base(currentContext, currentFactory)
    {
        _isRootState= true;
        InitializeSubState();
    }

    public override void CheckSwitchStates()
    {

    }

    public override void EnterState()
    {

    }

    public override void ExitState()
    {
        
    }

    public override void InitializeSubState()
    {
        if (_ctx.IsMovePressed)
        {
            SetSubState(_factory.Move());
        }
        else
        {
            SetSubState(_factory.Idle());
        }
    }

    public override void UpdateState()
    {
   
    }
}
