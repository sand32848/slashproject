using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpState : BaseState
{
    public JumpState(StateMachine currentContext, PlayerStateFactory currentFactory) : base(currentContext, currentFactory)
    {
        _isRootState= true;
        InitializeSubState();
    }

    public override void CheckSwitchStates()
    {
        if(_ctx.rb.velocity.y <= 0)
        {
            RaycastHit2D groundHit = Physics2D.Raycast(_ctx.transform.position, Vector2.down, 1f,LayerMask.GetMask("Ground"));

            if (groundHit)
            {
                SwitchState(_factory.Ground());
            }
        }
    }

    public override void EnterState()
    {
        _ctx.rb.AddForce(new Vector2(0, _ctx.jumpForce),ForceMode2D.Impulse);
    }

    public override void ExitState()
    {
        _ctx.isJumping = false;
    }

    public override void InitializeSubState()
    {
        if (!_ctx.IsMovePressed)
        {
            SetSubState(_factory.Idle());
        }
        else
        {
            SetSubState(_factory.Move());
        }
    }

    public override void UpdateState()
    {
        CheckSwitchStates();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
