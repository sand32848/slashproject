using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class StateMachine : MonoBehaviour
{

    private PlayerStateFactory _states;
    public CharacterController characterController { get; private set; }
    private BaseState _currentState;
    public BaseState CurrentState { get { return _currentState; } set { _currentState = value; } }

    public bool IsMovePressed { get; private set; }
    public Vector2 MoveDirection { get; private set; }
    public Rigidbody2D rb { get; private set; }
    private bool _isJumping;
    public bool isJumping { get { return _isJumping; }  set {_isJumping = value;} }
    [field: SerializeField] public float acceleRateSpeed { get; private set; }
    [field: SerializeField] public float maxSpeed { get; private set; }
    [field: SerializeField] public float jumpForce { get; private set; }

    void Awake()
    {
        _states = new PlayerStateFactory(this);

        _currentState = _states.Ground();
        _currentState.EnterState();
    }

    private void OnEnable()
    {
        GlobalInputController.Instance.playerAction.Player.Movement.started += OnMovement;
        GlobalInputController.Instance.playerAction.Player.Movement.performed += OnMovement;
        GlobalInputController.Instance.playerAction.Player.Movement.canceled += OnMovement;
        GlobalInputController.Instance.playerAction.Player.Fire.started += OnFire;
        GlobalInputController.Instance.playerAction.Player.Jump.started += OnJump;
    }

    private void OnJump(InputAction.CallbackContext context)
    {
        isJumping = true;
    }

    private void OnFire(InputAction.CallbackContext context)
    {
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        IsMovePressed = context.ReadValue<Vector2>().x != 0 || context.ReadValue<Vector2>().y !=0;
        MoveDirection = new Vector2(context.ReadValue<Vector2>().x, 0); 
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        _currentState.UpdateStates();
    }

    private void OnGUI()
    {
        string content = _currentState != null ? _currentState.GetType().Name : "(no current state)";
        GUILayout.Label($"<color='black'><size=40>{content}</size></color>");
    }
}
