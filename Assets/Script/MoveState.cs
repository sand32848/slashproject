using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class MoveState : BaseState
{
    public MoveState(StateMachine currentContext, PlayerStateFactory playerStateFactory) : base(currentContext, playerStateFactory)
    {


    }

    public override void CheckSwitchStates()
    {
        if (!_ctx.IsMovePressed)
        {
            SwitchState(_factory.Idle());
        }
    }

    public override void EnterState()
    {

    }

    public override void ExitState()
    {
        
    }

    public override void InitializeSubState()
    {

    }

    public override void UpdateState()
    {
        _ctx.rb.AddForce(_ctx.acceleRateSpeed * _ctx.MoveDirection);

        _ctx.rb.velocity = new Vector2(Mathf.Clamp(_ctx.rb.velocity.x, -_ctx.maxSpeed, _ctx.maxSpeed), _ctx.rb.velocity.y);

        CheckSwitchStates();
    }
}
