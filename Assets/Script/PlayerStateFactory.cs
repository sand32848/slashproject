
public class PlayerStateFactory 
{
    StateMachine _context;
    public PlayerStateFactory(StateMachine currentContext)
    {
        _context = currentContext;
    }

    public BaseState Idle() { return new IdleState(_context,this);}
    public BaseState Move() { return new MoveState(_context,this);}
    public BaseState Pucnh() { return new PunchState(_context, this); }
    public BaseState Ground() { return new GroundState(_context, this); }
    public BaseState Jump() { return new JumpState(_context, this); }
}
