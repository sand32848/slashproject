using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchState : BaseState
{
    public PunchState(StateMachine currentContext, PlayerStateFactory currentFactory) : base(currentContext, currentFactory)
    {
        _isRootState= true;
    }

    public override void CheckSwitchStates()
    {
        throw new System.NotImplementedException();
    }

    public override void EnterState()
    {
        throw new System.NotImplementedException();
    }

    public override void ExitState()
    {
        throw new System.NotImplementedException();
    }

    public override void InitializeSubState()
    {
        throw new System.NotImplementedException();
    }

    public override void UpdateState()
    {
        throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
