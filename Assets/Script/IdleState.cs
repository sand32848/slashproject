using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class IdleState : BaseState
{
    public IdleState(StateMachine currentContext, PlayerStateFactory playerStateFactory) : base(currentContext, playerStateFactory)
    {

    }

    public override void CheckSwitchStates()
    {
        if (_ctx.IsMovePressed)
        {
            SwitchState(_factory.Move());
        }
    }

    public override void EnterState()
    {

    }

    public override void ExitState()
    {
     
    }

    public override void InitializeSubState()
    {

    }

    public override void UpdateState()
    {
        _ctx.rb.velocity = new Vector2(0, _ctx.rb.velocity.y);
        CheckSwitchStates();
    }
}
