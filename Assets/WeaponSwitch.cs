using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class WeaponSwitch : MonoBehaviour
{
    [SerializeField] private BaseState currentWeaponState;

    private GameObject currentWeapon;
    [SerializeField] private List<GameObject> weaponList= new List<GameObject>();

    public static Action OnWeaponChange;

    private void OnEnable()
    {
        GlobalInputController.Instance.playerAction.Player.WeaponChange.started += WeaponChange;
    }

    private void OnDisable()
    {
        GlobalInputController.Instance.playerAction.Player.WeaponChange.started -= WeaponChange;

    }

    public void WeaponChange(InputAction.CallbackContext context)
    {
        print(context.ReadValue<float>() - 1f);
        float weaponIndex = context.ReadValue<float>() - 1f;

        weaponList.ForEach(i => i.SetActive(false));
        currentWeapon = weaponList[(int)weaponIndex];
        currentWeapon.SetActive(true);
    }

}
