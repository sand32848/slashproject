using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float attackRange;
    [SerializeField] private int attackDaamge;
    [SerializeField] private float attackCooldown;

    [SerializeField] private List<Rigidbody2D> gib;
    [SerializeField] private GameObject bloodParicle;
    private GameObject player => GameObject.FindGameObjectWithTag("Player");

    private Health health => GetComponent<Health>();

    private void Start()
    {
        health.OnDeath.AddListener(spawnGib);
    }

    public void spawnGib()
    {
        print(directionToPlayer());
        Destroy(gameObject);

        for(int i =0; i < gib.Count; i++)
        {
          Rigidbody2D rb = Instantiate(gib[i],transform.position,Quaternion.identity);

            rb.AddForce(new Vector2(-5, 7),ForceMode2D.Impulse);
        }

        int dir = directionToPlayer().x > 0.1? -1 : 1;

        GameObject particle = Instantiate(bloodParicle, transform.position, Quaternion.identity);

        particle.transform.localScale = new Vector3(dir, 1, 1);
    }

    private Vector2 directionToPlayer()
    {
        Vector2 playerDir = player.transform.position - transform.position;

        return playerDir.normalized;
    }

    private float distanceFromPlayer()
    {
       return Vector2.Distance(transform.position,player.transform.position);
    }

}
