using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public int health;

    public UnityEvent OnDeath;

    public void reduceHealth(int damage)
    {
        health -= damage;

        if(health <= 0)
        {
            OnDeath?.Invoke();
        }
    }
}
